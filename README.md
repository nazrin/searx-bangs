# searx-bangs

## Installing and Running
* `$ git clone https://codeberg.org/nazrin/searx-bangs`
* Edit `searx-bangs.js` and set `inPort` `outPort` and add custom bangs to `bangs` if you want
* Setup nginx to reverse proxy
* `$ node searx-bangs.js`

Nginx (replace `INPORT`):
```nginx
location / {
	proxy_set_header X-Forwarded-For $remote_addr;
	proxy_pass http://localhost:INPORT;
}
```

Searx (replace `OUTPORT`)
```yaml
server:
  port : OUTPORT
```

## A Hart
a hart ♥

