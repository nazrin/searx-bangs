let http        = require("http");
let querystring = require("querystring");
let url         = require("url");

let inPort  = 1489;
let outPort = 8888;

let bangs  = JSON.parse(require("fs").readFileSync("./p_bangs.json").toString());
bangs.yimg = "https://yandex.com/images/search?text={{{s}}}";

let openSearch = ``;

function redirect(res, newUrl){
	res.writeHead(302, {
		Location: newUrl
	});
	res.end();
}
function tryBanging(res, q){
	let [_, term, query] = q.match(/!([\w\u0400-\u0500]+) ?(.*)/u) || [];
	let bang = bangs[term];
	if(bang){
		let newUrl = bang.replace("{{{s}}}", querystring.escape(query));
		redirect(res, newUrl);
		return true; // hehe see ya later
	}
	return false; // tfw
}
function forwardRequest(parsedURL, req, res){
	let pReq = http.request({
		port: outPort,
		host: req.headers["host"].match("[^:]+")[0],
		method: req.method,
		headers: req.headers,
		path: parsedURL.pathname + (parsedURL.search ? parsedURL.search : "")
	});
	pReq.addListener("response", pRes => {
		pRes.on("data", (chunk) => {
			res.write(chunk, "binary");
		});
		pRes.on("end", () => {
			res.end();
		});
		if(req.headers["origin"]){
			pRes.headers["access-control-allow-origin"] = req.headers["origin"];
		}
		res.writeHead(pRes.statusCode, pRes.headers);
	});
	pReq.on("error", err => {
		console.error(err);
		res.statusCode = "404";
		res.end();
	});
	req.addListener("data", chunk => {
		pReq.write(chunk, "binary");
	});
	req.addListener("end", () => {
		pReq.end();
	});
}
function respondWithText(req, res, text){
	res.write(text);
	res.end();
}

http.createServer((req, res) => {
	let parsedURL = url.parse(req.url);
	let shouldForward = true;
	try{
		if(parsedURL.path.startsWith("/opensearch.xml"))
			return respondWithText(req, res, openSearch);
		if(parsedURL.path.startsWith("/search")){
			if(req.method == "GET"){
				let q = querystring.decode(parsedURL.query).q;
				shouldForward = q && !tryBanging(res, q);
			} else if(req.method == "POST"){
				shouldForward = false;
				req.on("data", d => {
					if(!d || !d.length)
						return;
					let data = d.toString();
					let q = querystring.decode(d).q;
					if(!q || tryBanging(res, q))
						redirect(res, `/search?${data}`);
				});
			}
		}
	} catch(err){
		console.error(err)
	}
	if(shouldForward)
		forwardRequest(parsedURL, req, res);
}).listen(inPort);

